import React from 'react'
import { MenuItem } from 'semantic-ui-react'
import {IChannel} from '../../models/Chanel'


interface IProps {
    channel: IChannel
}

export const ChannelItem: React.FC<IProps> = ({channel}) => {
    return (
        <MenuItem
            key={channel.id}
            onClick={() => console.log(channel.name)}
            name={channel.name}
            style={{opacity: 0.7}}
        >
        # {channel.name}
        </MenuItem>
    )
}
