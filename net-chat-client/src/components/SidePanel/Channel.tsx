import React, { useState, useEffect } from 'react';
import { Icon, Menu } from 'semantic-ui-react';

import { IChannel } from '../../models/Chanel';
import { ChannelItem } from './ChannelItem';
import { ChannelForm } from './ChannelForm';
import Channels from '../../api/services/ChannelService';


const Channel = () => {
	const [channels, setChannels] = useState<IChannel[]>([]);
	const [statusModal, setStatusModal] = useState(false);

	useEffect(() => {

		Channels.list().then((response) => {
			setChannels(response);
		});
	}, []);

	const handleModal = () => setStatusModal(!statusModal);

	const displayChannels = (channels: IChannel[]) => {
		return (
			channels.length > 0 &&
			channels.map((channel) => (
				<ChannelItem key={channel.id} channel={channel} />
			))
		);
	};

    const handleCreateChannel = (channel: IChannel) => {
		Channels.create(channel).then(() => setChannels([...channels, channel]))
    }

	return (
		<React.Fragment>
			<Menu.Menu style={{ paddingBotton: '2em' }}>
				<Menu.Item>
					<span>
						<Icon name="exchange" /> CHANNELS
					</span>{' '}
					({channels.length}) <Icon name="add" onClick={handleModal} />
				</Menu.Item>
				{displayChannels(channels)}
			</Menu.Menu>
			<ChannelForm statusModal={statusModal} handleModal={handleModal} createChannel={handleCreateChannel}/>
		</React.Fragment>
	);
};

export default Channel;
