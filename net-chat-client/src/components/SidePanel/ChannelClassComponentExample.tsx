import React, {Component} from 'react'
import { Button, Form, Icon, Input, Menu, MenuItem, Modal } from 'semantic-ui-react'
import axios from 'axios'

import {IChannel} from '../../models/Chanel'
import { ChannelItem } from './ChannelItem'

interface IState {
    channels: IChannel[],
    modal: boolean
}

class ChannelClassComponentExample extends Component<{}, IState> {

    state: IState = {
        channels: [],
        modal: false
    }

    endPointPath: string = "http://localhost:27010/api/channels"

    componentDidMount() {
        axios.get<IChannel[]>(this.endPointPath)
            .then((response) => {
                this.setState({channels: response.data})
            })
    }

    handleModal = () => this.setState({modal: !this.state.modal})

    displayChannels = (channels: IChannel[]) => {
        return(
           channels.length > 0 &&  
           channels.map((channel) => (
               <ChannelItem key={channel.id} channel={channel}/>
           ))
        )
    }

    render() {
        const {channels, modal} = this.state
        return (
            <React.Fragment>
            <Menu.Menu
                style={{paddingBotton: '2em'}}
            >
                <Menu.Item>
                    <span>
                        <Icon name='exchange'/> CHANNELS
                    </span> ({channels.length}) <Icon name='add' onClick={this.handleModal} />
                </Menu.Item>
                {this.displayChannels(channels)}
            </Menu.Menu>
            <Modal
                basic
                open={modal}
            >
                <Modal.Header>Add Channel</Modal.Header>
                <Modal.Content>
                    <Form>
                        <Form.Field>
                            <Input fluid label="Channel Name" name="channelName" />
                        </Form.Field>
                        <Form.Field>
                            <Input fluid label="Description" name="channelDescription" />
                        </Form.Field>
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button basic color='green' inverted>
                        <Icon name='checkmark' /> Add
                    </Button>
                    <Button basic color='red' inverted onClick={this.handleModal}>
                        <Icon name='remove' /> Cancel
                    </Button>
                </Modal.Actions>
            </Modal>
            </React.Fragment>
        )
    }
}

export default ChannelClassComponentExample
