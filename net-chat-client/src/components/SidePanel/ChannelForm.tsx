import React, {ChangeEvent, useState} from 'react'
import { Button, Form, Icon, Input, Modal } from 'semantic-ui-react'
import { IChannel } from '../../models/Chanel'
import {v4 as uuid} from 'uuid'

interface IProps {
    statusModal: boolean,
    handleModal: () => void,
    createChannel: (channel: IChannel) => void
}

export const ChannelForm: React.FC<IProps> = ({statusModal, handleModal, createChannel}) => {

    const initialChannel : IChannel = {
        id: '',
        name: '',
        description: ''
    }

    const [channel, setChannel] = useState<IChannel>(initialChannel)

    const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        setChannel({...channel, [event.target.name]: event.target.value })
    }

    const handleSubmitForm = () => {
        let newChannel: IChannel = {
            ...channel,
            id: uuid()
        }
        createChannel(newChannel)
        handleModal();
    }

    return (
        <Modal
                basic
                open={statusModal}
            >
                <Modal.Header>Add Channel</Modal.Header>
                <Modal.Content>
                    <Form>
                        <Form.Field>
                            <Input fluid label="Channel Name" onChange={handleInputChange} name="name" />
                        </Form.Field>
                        <Form.Field>
                            <Input fluid label="Description" onChange={handleInputChange} name="description" />
                        </Form.Field>
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button basic color='green' inverted onClick={handleSubmitForm}>
                        <Icon name='checkmark' /> Add
                    </Button>
                    <Button basic color='red' inverted onClick={handleModal}>
                        <Icon name='remove' /> Cancel
                    </Button>
                </Modal.Actions>
            </Modal>
    )
}
