import React from 'react'
import { CommentGroup, Segment } from 'semantic-ui-react'
import { MessageForm } from './MessageForm'
import { MessageHeader } from './MessageHeader'

const Messages = () => {
    return (
        <React.Fragment>
            <MessageHeader />
            <Segment>
                <CommentGroup>

                </CommentGroup>
            </Segment>
            <MessageForm />
        </React.Fragment>            
    )
}


export default Messages