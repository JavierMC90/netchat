import React from 'react'
import { Header, HeaderSubheader, Icon, Segment } from 'semantic-ui-react'
import { SearchInput } from './SearchInput'

export const MessageHeader = () => {
    return (
        <Segment clearing>
            <Header fluid="true" as="h2" floated="left" style={{marginBottom: 0}}>
                <span>
                Channel
                <Icon name="star outline" color="black" />
                </span>
                <HeaderSubheader>2 Users</HeaderSubheader>
            </Header>
            <SearchInput />
        </Segment>
    )
}
