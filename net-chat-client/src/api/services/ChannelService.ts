import { IChannel } from '../../models/Chanel'
import requestType from '../agent'

const Channels = {
    list: (): Promise<IChannel[]> => requestType.get('/channels'),
    create: (channel: IChannel) => requestType.post('/channels', channel)
}

export default Channels