﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using NetChat.Application.Channels;
using NetChat.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NetChat.API.Controllers
{
    [ApiController]
    [Route("api/channels")]
    public class ChannelsController : ControllerBase
    {
        private readonly IMediator _mediator;
        public ChannelsController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet]
        public async Task<ActionResult<List<Channel>>> List() => await _mediator.Send(new List.Query());
        
        [HttpGet("{id}")]
        public async Task<ActionResult<Channel>> Details(Guid Id) =>  await _mediator.Send(new Details.Query { Id = Id });

        [HttpPost]
        public async Task<Unit> Create(Create.Command command)
        {
            return await _mediator.Send(command);
        }

    }
}
